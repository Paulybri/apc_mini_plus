# APC_MINI_PLUS
A Midi Remote script for controlling Ableton Live 10/11 using an APC mini. It might also work on Ableton Live 9 but I have not tested it.

## Demo
https://youtu.be/Rrd3BDDvSlc

## Features
### Session management
- **Delete** clips by holding `SHIFT` and pressing on the desired clip
- **Copy** a clip to another slot by holding down `SHIFT`, holding down the desired clip to copy and pressing an empty clip slot.
- **Record** clips automatically by pressing an empty clip slot, even if the track was not previously armed.
- **Overdub** a clip by double pressing it
- **Undo** any action by holding `SHIFT` and pressing the button *above* the `STOP ALL CLIPS` button
- **Pan** a track with the slider when `SHIFT` is pressed down

### Metronome
- Toggle the **Metronome** by holding `SHIFT` and pressing the button *above* the `UNDO` button 
- Tap the **Tempo** by holding the `METRONOME` button down and tapping any button in the main 8x8 grid

### Advanced Menu
Open the advanced menu by double pressing the `SHIFT` button. It is exited when pressing `SHIFT` again. This menu allows you to:
- Activate  a **Fixed recording time** by pressing the red pad in the upper right corner
- Set the **Fixed recording length** by pressing one of the pads under the activation pad
    - The possible lengths are respectively `1, 2, 4, 8, 16, 32 and 64` bars
- **Arm** a track by pressing it's bottom row button

More functionnalities might come in the future. I am open to suggestions!

## Installation
Install the script by doing the following:
- Close Ableton Live
- Download this repository as `zip`
- Extract the `zip` and rename the extracted folder to `apc_mini_plus`
- Copy the entire `apc_mini_plus` folder to your **Ableton Midi Remote Scripts folder**, it is usually found here:
    - `**PATH_TO_ABLETON**\Live 10 Suite\Resources\MIDI Remote Scripts`
- Your `MIDI Remote Scripts` folder should look like this:
  <p align="left"><img src="README images/midi_remote_script.JPG" /></p>
- Start Ableton Live
- In `Live->Preferences` configure the control surface as follows:
  <p align="left"><img src="README images/midi_setup.JPG" /></p>
- Enjoy!
## Thank You
A big thanks to [markharwood](https://github.com/markharwood/MH_APC_mini) and [JOJ0](https://github.com/JOJ0/ableton-live9-remote-scripts/tree/master/APC_mini_jojo), their work helped me out alot to get things up and running
